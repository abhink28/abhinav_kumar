package main

import (
	"encoding/json"
	"fmt"
	"infobook/profiles"
	"log"
	"net/http"
	"net/mail"
	"net/url"
	"sync"

	"golang.org/x/net/context"
)

// Generic response type used by ServeHTTP functions.
type response struct {
	Data     interface{} `json:"data"`
	Token    string      `json:"token"`
	ErrorMsg string      `json:"error"`
}

// Options to configure handler behavior.
type handlerOpts struct {
	CheckAccess   bool
	SkipXSRF      bool
	PopulateToken bool
	RefreshToken  bool // unused
}

// endpoint is type that wraps around a function that must always be called
// post user authentication. An endpoint will always check for valid user session
// but it can be configured to ignore/refresh xsrf check.
type endpoint struct {
	Handler func(context.Context, *http.Request) (interface{}, error)
	Opts    handlerOpts
}

// type endpoint func(context.Context, *http.Request) (interface{}, error)

func (e *endpoint) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("X-Frame-Options", "SAMEORIGIN")

	if err := r.ParseForm(); err != nil {
		SetErrorResponse(w, "cannot parse form data", http.StatusInternalServerError)
	}
	access := checkAccess(context.Background(), w, r, e.Opts.SkipXSRF)
	if !access {
		SetErrorResponse(w, "access denied", http.StatusUnauthorized)
		return
	}

	data, err := e.Handler(context.Background(), r)
	if err != nil {
		s := fmt.Sprintf("error encountered: %s", err.Error())
		SetErrorResponse(w, s, http.StatusInternalServerError)
		return
	}

	token := r.Form.Get("token")
	if e.Opts.PopulateToken {
		email := r.Form.Get("userid")
		if email != "" {
			token = getXSRF(email)
		}
	}

	b, err := json.Marshal(&response{
		Data:  data,
		Token: token,
	})
	if err != nil {
		SetErrorResponse(w, "error fetching user", http.StatusInternalServerError)
		return
	}

	w.Write(b)
}

// entrypoint is a handler that initiates an application's workflow. entrypoint has the
// reponsibility of setting up session and xsrf token. If required, an entrypoint may be
// configured to perform access checks as well.
// TODO: Design a better type for handlers that may lie in the middle.
type entrypoint struct {
	Handler func(context.Context, *http.Request) (interface{}, error)
	Opts    handlerOpts
}

func (e *entrypoint) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("X-Frame-Options", "SAMEORIGIN")

	if err := r.ParseForm(); err != nil {
		SetErrorResponse(w, "cannot parse form data", http.StatusInternalServerError)
	}

	// Entry point may still require access checks.
	if e.Opts.CheckAccess {
		access := checkAccess(context.Background(), w, r, e.Opts.SkipXSRF)
		if !access {
			SetErrorResponse(w, "access denied", http.StatusUnauthorized)
			return
		}
	}

	if err := setSession(context.Background(), w, r); err != nil {
		s := fmt.Sprintf("error: %s", err)
		SetErrorResponse(w, s, http.StatusInternalServerError)
		return
	}

	token := setXSRF(r.FormValue("email"))

	data, err := e.Handler(context.Background(), r)
	if err != nil {
		if err == ErrUnauthorized {
			SetErrorResponse(w, "access denied", http.StatusUnauthorized)
			return
		}
		s := fmt.Sprintf("error: %s", err)
		SetErrorResponse(w, s, http.StatusInternalServerError)
		return
	}

	b, err := json.Marshal(&response{
		Data:  data,
		Token: token,
	})
	if err != nil {
		SetErrorResponse(w, "error fetching user", http.StatusInternalServerError)
		return
	}

	w.Write(b)
}

// entrypoint. Checks user's credentials.
func authHandler(ctx context.Context, r *http.Request) (interface{}, error) {
	u, p, ok := r.BasicAuth()
	if !ok || !profiles.CheckAuth(ctx, u, p) {
		return nil, ErrUnauthorized
	}
	return &profiles.User{Email: u}, nil
}

// entrypoint. Creates/registers a new user.
func createHandler(ctx context.Context, r *http.Request) (interface{}, error) {
	email := r.Form.Get("email")
	pass := r.Form.Get("pass")

	if _, err := mail.ParseAddress(email); err != nil {
		return nil, fmt.Errorf("invalid email address: %v", err)
	}

	u, err := profiles.RegisterUser(ctx, email, pass, false)
	if err != nil {
		return nil, err
	}
	return u, nil
}

// endpoint. Fetches user profile information.
func profileHandler(ctx context.Context, r *http.Request) (interface{}, error) {
	email := r.Form.Get("email")
	u, err := profiles.GetUserProfile(ctx, email)
	if err != nil {
		fmt.Errorf("error getting profile for: %s", email)
	}
	return u, nil
}

// endpoint. Updates user profile information.
func updateHandler(ctx context.Context, r *http.Request) (interface{}, error) {
	user := &profiles.User{
		r.Form.Get("email"),
		r.Form.Get("name"),
		r.Form.Get("address"),
		r.Form.Get("phone"),
	}
	newID := r.Form.Get("email")
	oldID := r.Form.Get("oldemail")

	if oldID != newID {
		if _, err := mail.ParseAddress(newID); err != nil {
			log.Printf("invalid email address: %v", err)
			return nil, fmt.Errorf("invalid email address: %v", err)
		}

		_, err := profiles.ReRegisterUser(ctx, oldID, newID)
		if err != nil {
			log.Printf("error updating credentials: %v", err)
			return nil, err
		}
	}

	u, err := profiles.UpdateUser(ctx, user, oldID)
	if err != nil {
		return nil, err
	}
	return u, nil
}

// Specific type to deal with direct template based responses.
type templateResponse struct {
	LoginURL string
	Email    string
	Token    string
	Error    string
}

// Loads the log in page of the application.
func loginHandler(w http.ResponseWriter, r *http.Request) {
	param := templateResponse{
		LoginURL: profiles.GetLoginURL(),
	}
	if err := tmpl.ExecuteTemplate(w, "main.html", param); err != nil {
		log.Fatal(err)
	}
}

// Handles oauth authentication.
func oauthHandler(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	param := templateResponse{}

	if err := r.ParseForm(); err != nil {
		param.Error = "Invalid form data."
		return
	}

	code := r.Form.Get("code")

	// TODO: Verify state!!!
	// state := r.Form.Get("state")

	defer func() {
		if err := tmpl.ExecuteTemplate(w, "main.html", param); err != nil {
			log.Fatal(err)
		}
	}()

	user, err := profiles.RegisterOAuthUser(ctx, code)
	if err != nil {
		param.Error = "User not authorised. Please try login with your email ID."
		return
	}

	if err := setSessionValue(ctx, w, r, user.Email); err != nil {
		param.Error = "Internal error."
		return
	}

	token := setXSRF(user.Email)

	param.Email = user.Email
	param.Token = token
}

// Log a user out, clearing up their session and tokens.
func logoutHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("X-Frame-Options", "SAMEORIGIN")

	if err := r.ParseForm(); err != nil {
		SetErrorResponse(w, "internal error", http.StatusInternalServerError)
		return
	}

	var wg sync.WaitGroup

	wg.Add(2)

	go func() {
		defer wg.Done()

		clearSession(context.Background(), w, r)
	}()

	go func() {
		defer wg.Done()

		u := r.Form.Get("email")
		clearXSRF(u)
	}()

	wg.Wait()

	http.Redirect(w, r, "/", http.StatusFound)
}

// Initiates a password reset response. This is also responsible for sending out
// password reset email.
func resetInitHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("X-Frame-Options", "SAMEORIGIN")

	ctx := context.Background()

	if err := r.ParseForm(); err != nil {
		SetErrorResponse(w, "internal error", http.StatusInternalServerError)
		return
	}

	receip := r.Form.Get("email")

	t, err := profiles.GetUserAuthType(ctx, receip)
	if err != nil {
		SetErrorResponse(w, err.Error(), http.StatusUnauthorized)
		return
	}

	if t == "GOOGLE" {
		SetErrorResponse(w, "oauth type does not support password", http.StatusUnauthorized)
		return
	}

	if err := profiles.SendResetEmail(ctx, receip); err != nil {
		log.Print("unable to send email")
		SetErrorResponse(w, "internal error", http.StatusInternalServerError)
		return
	}
	w.Write([]byte(`{"data": "email sent"}`))
}

// Handles the form for reset password.
func resetFormHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("X-Frame-Options", "SAMEORIGIN")

	errStr := ""
	if err := r.ParseForm(); err != nil {
		errStr = "Error: Invalid Operation."
	}
	code := r.Form.Get("code")

	param := struct {
		Code  string
		Error string
	}{
		Code:  code,
		Error: errStr,
	}
	if err := tmpl.ExecuteTemplate(w, "reset.html", param); err != nil {
		log.Fatal(err)
	}
}

// Saves new user password.
// TODO: Make code design uniform between similar functions (like above, which does not
// use deferred template generation).
func resetHandler(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()

	param := struct {
		Error string
	}{}

	if err := r.ParseForm(); err != nil {
		param.Error = "Error: Invalid Operation."
		return
	}

	defer func() {
		if err := tmpl.ExecuteTemplate(w, "reset_done.html", param); err != nil {
			log.Fatal(err)
		}
	}()

	user := r.Form.Get("user_mail")
	pass := r.Form.Get("user_password")
	reps := r.Form.Get("user_password_repeate")

	code, err := url.QueryUnescape(r.Form.Get("code"))
	if err != nil {
		param.Error = "Error: Could not parse code."
		return
	}

	if pass != reps {
		param.Error = "Password mismatch. Try again."
		return
	}

	if err := profiles.VerifyResetCode(ctx, user, code); err != nil {
		param.Error = "Invalid code."
		return
	}

	if err := profiles.ChangePassword(ctx, user, pass); err != nil {
		param.Error = "Unable to change password."
		return
	}

	go func() {
		if err := profiles.ClearResetCode(ctx, user); err != nil {
			log.Print("!-> user reset code not performed: %s", user)
		}
	}()
}

// This handler is required to get xsrf token to perform profile updates once
// a profile has been opened in a browser. If a user has valid session, a profile is loaded
// without checking the xsrf toekn. This enables parallel browsing. However, if a
// logged in user then clicks update button they are sent the current session xsrf token. This
// prevents direct access to profile update view.
func tokenHandler(ctx context.Context, r *http.Request) (interface{}, error) {
	// TODO: Add Header Origin/Referer checks.
	return "", nil
}
