// Handles authentication related logic.
package profiles

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"golang.org/x/crypto/bcrypt"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// env is to select various configurations based on application environment.
var env = ""

var conf = &oauth2.Config{
	ClientID:     clientID,
	ClientSecret: clientSecret,
	RedirectURL:  "http://127.0.0.1:5000/oauthorise",
	Scopes: []string{
		"https://www.googleapis.com/auth/userinfo.email",
	},
	Endpoint: google.Endpoint,
}

func init() {
	env = os.Getenv("ENV")
	if env == "" {
		env = "prod"
	}

	log.Print("Env set: ", env)
	if env == "prod" {
		conf.RedirectURL = "https://stark-fjord-40589.herokuapp.com/oauthorise"
	}
}

// getCredentials returns user's userId, password and login type (LOGIN/GOOGLE).
func getCredentials(ctx context.Context, userID string) (string, string, string, error) {
	var u, p, t string
	if err := db.QueryRow("SELECT * FROM credentials WHERE userid = ?", userID).
		Scan(&u, &p, &t); err != nil {
		if err != sql.ErrNoRows {
			log.Print("error fetching row: ", err)
			return "", "", "", err
		}
		log.Print("No rows found.")
		return "", "", "", nil
	}
	return u, p, t, nil
}

func CheckAuth(ctx context.Context, userID, pass string) bool {
	_, p, t, err := getCredentials(ctx, userID)
	if err != nil {
		log.Print("error getting credentials: ", err)
		return false
	}
	if t == "GOOGLE" {
		log.Print("credentials type oauth: ", err)
		return false
	}
	if err := bcrypt.CompareHashAndPassword([]byte(p), []byte(pass)); err != nil {
		return false
	}
	return true
}

func GetUserAuthType(ctx context.Context, userID string) (string, error) {
	_, _, t, err := getCredentials(ctx, userID)
	if err != nil {
		log.Print("error getting credentials: ", err)
		return "", fmt.Errorf("error getting credentials")
	}
	if t == "" {
		return "", fmt.Errorf("user not found: %s", userID)
	}
	return t, nil
}

func CheckOAuth(ctx context.Context, code string) ([]byte, bool) {
	tok, err := conf.Exchange(oauth2.NoContext, code)
	if err != nil {
		log.Print("OAuth code exchange failed: ", err)
		return nil, false
	}
	// Construct the client.
	client := conf.Client(oauth2.NoContext, tok)
	resp, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
	if err != nil {
		log.Print("OAuth client fetch failed: ", err)
		return nil, false
	}
	defer resp.Body.Close()
	data, _ := ioutil.ReadAll(resp.Body)
	return data, true
}

func ChangePassword(ctx context.Context, user, pass string) error {
	hash, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
	if err != nil {
		log.Print("error creating account: ", err)
		return fmt.Errorf("error creating user account")
	}

	_, err = updateCredentialsStmt.Exec(user, hash, "LOGIN", user)
	if err != nil {
		log.Print("error updating user credentials: ", err)
		return fmt.Errorf("error updating user account")
	}
	return nil
}
