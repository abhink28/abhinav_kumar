// Manages password reset logic.
// TODO: Clean the below code.
package profiles

import (
	"bytes"
	"fmt"
	"log"
	"net/smtp"
	"net/url"

	"golang.org/x/net/context"
)

// SendResetEmail sends out a password reset email to the user. It also manages
// reset code generation. Email is currently being sent using a free gmail account.
// TODO: Move reset code generation/storage out of this function.
func SendResetEmail(ctx context.Context, userID string) error {
	code := randToken()
	host := "http://localhost:5000"
	if env == "prod" {
		host = "https://stark-fjord-40589.herokuapp.com"
	}
	resetURL := host + `/resetform?code=` + code

	// TODO: Handle duplicate row error.
	_, err := db.Exec("INSERT INTO resetrequest(email, code) VALUES(?, ?)", userID, code)
	if err != nil {
		log.Print("cannot create reset code", err)
		return fmt.Errorf("unable to creat reset password link")
	}

	auth := smtp.PlainAuth(
		"",
		"abhink.test@gmail.com",
		"smtpmailtest",
		"smtp.gmail.com",
	)

	err = smtp.SendMail(
		"smtp.gmail.com:25",
		auth,
		"abhink.test@gmail.com",
		[]string{userID},
		[]byte("To: "+userID+"\r\n"+
			"Subject: Infobook reset password request\r\n"+
			"\r\n"+
			"Click here to reset password: "+resetURL+"\r\n"),
	)
	if err != nil {
		log.Print("unable to send email", err)
		_, err := db.Exec("DELETE FROM resetrequest WHERE email = ?", userID)
		if err != nil {
			log.Printf("cannot delete reset code for %s: ", userID, err)
		}
		return fmt.Errorf("unable to send reset password mail")
	}
	return nil
}

func VerifyResetCode(ctx context.Context, user, code string) error {
	var u string
	var c []byte

	if err := db.QueryRow("SELECT * FROM resetrequest WHERE email = ?", user).Scan(&u, &c); err != nil {
		log.Print("error fetching code: ", err)
		return fmt.Errorf("error fetching code for %s", user)
	}

	codeStr := string(bytes.Trim(c, "\x00"))

	codeStr, err := url.QueryUnescape(codeStr)
	if err != nil {
		log.Print("could not parse code.")
		return fmt.Errorf("invalid code - parse error")
	}

	if codeStr != code {
		log.Printf("CODE:  %+#v %+#v %+#v %+#v ", code, string(c), string(bytes.TrimSpace(c)), len(code))
		log.Print("invalid code")
		return fmt.Errorf("invalid code")
	}

	return nil
}

// ClearResetCode removes a users reset code form the database once their password has been
// reset.
// TODO: Add rety logic.
func ClearResetCode(ctx context.Context, user string) error {
	if _, err := db.Exec("DELETE FROM resetrequest WHERE email = ?", user); err != nil {
		log.Print("failed to delete user reset code")
		return fmt.Errorf("failed to delete user reset code")
	}
	return nil
}
