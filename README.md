# README #

Go based application as per this [requirement spec](https://docs.google.com/document/d/1fnjPawehvP7jEjOfoEUOIp-J4tR1_y1iPyvFGEUfHjo/edit?usp=sharing)

App specification:

*Backed:*
* Go: Standard `net/http` library used. `gorilla/session` used for session management.
* Several other Go libraries used as per convenience.

*Frontend:*
Google closure library used to deliver a single page like App. SPA like because the application
currently does not have any history management.

JS code has not been minified in the application yet. There is also a strange closure specific error where
`deps.js` is automatically added to list of scripts, causing a minor JS error (no effect on app functionality).

*Authentication/Security:*

1. The application is deployed on heroku which provides ssl connections by default. Therefore the application itself does not implement secure listeners.
2. Due to above reason, basic auth is used to identify a user.
3. For email based login, user ID's (user's email addresses) and passwords are stored as hash in the database.
4. Each request is sent out with a client side signed session.
5. CSRF token is also added to an authorized response, and checked in proceeding requests wherever required.


*Main TODO's:*

1. Improve CSRF token system (check Origin/Referef header, try per request token generation. Add token age).
2. Add a more robust CSRF token store (currently an in memory map.)
3. Look into server side session persistence.
4. Improve general code layout/design.
5. JS/input validations.